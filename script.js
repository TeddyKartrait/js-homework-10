let arrStudents = [{
    name: 'Евгений',
    surname: 'Старущенко',
    middleName: 'Андреевич',
    faculty: 'Механик',
    DOB: '1999-01-26',
    startLern: '2014-05-01',
},
{
    name: 'Джек',
    surname: 'Керуак',
    middleName: 'Петрович',
    faculty: 'Опричник',
    DOB: '1998-04-13',
    startLern: '2022-06-01',
},
{
    name: 'Джон',
    surname: 'Корнуэл',
    middleName: 'Валентинович',
    faculty: 'Мясник',
    DOB: '1996-08-18',
    startLern: '2020-04-01',
},];

const form = document.createElement('form')
const container = document.getElementById('container')
const btn = document.createElement('button')
const table = document.createElement('table')
const tbody = document.createElement('tbody')
const filterWrapper = document.createElement('div')
const inpFilFio = document.createElement('input')
const inpFilStartLern = document.createElement('input')
const inpFilFaculty = document.createElement('input')
const inpFilEndLern = document.createElement('input')


filterWrapper.id = 'filterWrapper'
inpFilFio.placeholder = 'Поиск по ФИО'
inpFilFaculty.placeholder = 'Поиск по факультету'
inpFilStartLern.placeholder = 'Поиск по дате начала учебы'
inpFilEndLern.placeholder = 'Поиск по дате окончания учебы'
inpFilFio.classList.add('inp-search')
inpFilFaculty.classList.add('inp-search')
inpFilStartLern.classList.add('inp-search')
inpFilEndLern.classList.add('inp-search')
table.id = 'table'
table.setAttribute('border', '1')
table.setAttribute('cellspacing', '0')
table.setAttribute('cellpadding', '5')
form.id = 'form'
btn.id = 'btn'
btn.textContent = 'Добавить студента'
btn.type = 'button'
container.append(form)

function createTable () {
  i = 1;
  const tr = document.createElement('tr')
  while (i <= 4) {
    const th = document.createElement('th')
    tr.append(th)
    th.id = 'heading' + i
      switch (i) {
        case (1):
          th.textContent = 'ФИО учащегося'
          break;
        case (2):
          th.textContent = 'Факультет'
          break;
        case (3):
          th.textContent = 'Дата рождения и возраст'
          break;
        case (4):
          th.textContent = 'Годы обучения и курс'
          break;
      }
    i++
  }
 table.append(tr)
 container.append(table)
}
createTable();

function getYear(date) {
  const ageDifMs = Date.now() - date.getTime();
  const ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function renderTime (date) {
  const endLern = date.getTime() + 126144000000;
  const resultTime = new Date(endLern)
  var dd = resultTime.getDate();
    if (dd < 10) dd = '0' + dd;

  var mm = resultTime.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

  var yy = resultTime.getFullYear();
    if (yy < 10) yy = '0' + yy;

  return yy + '-' + mm + '-' + dd;
}

function cours (date) {
  const res = new Date().getFullYear() - date.getFullYear()
    if (res > 4) {
      return 'Закончил'
    } 
  return res
}

function createTr (item) {
  const tr = document.createElement('tr')
  let i = 0;
  while (i <= 3) {
    const td = document.createElement('td')
    td.classList.add('col')
    tr.append(td)
      switch (i) {
        case (0):
          td.textContent = item.FIO
          break;
        case (1):
          td.textContent = item.faculty
          break;
        case (2):
          td.textContent = item.DOB + ' Возраст: ' + getYear(new Date(item.DOB)) + ' лет'
          break;
        case (3):
          td.textContent = item.startLern + ' - ' + item.endLern + ' Курс: ' + cours(new Date(item.startLern))
        break;
      }
    i++
  }
  tbody.append(tr)
return tr
}

function createStr (arr) {
  let copyArr = [...arr];
  tbody.innerHTML = ''

  for (item of copyArr) {
    item.FIO = item.surname + ' ' + item.name + ' ' + item.middleName
    item.endLern = renderTime(new Date(item.startLern))
  }

  if (inpFilFio !== '') {
    copyArr = copyArr.filter(function (item) {
    if (item.FIO.includes(inpFilFio.value)) return true
    })
  }

  if (inpFilFaculty !== '') {
    copyArr = copyArr.filter(function (item) {
    if (item.faculty.includes(inpFilFaculty.value)) return true
    })
  }

  if (inpFilStartLern !== '') {
    copyArr = copyArr.filter(function (item) {
    if (item.startLern.includes(inpFilStartLern.value)) return true
    })
  }

  if (inpFilEndLern !== '') {
    copyArr = copyArr.filter(function (item) {
    if (item.endLern.includes(inpFilEndLern.value)) return true
    })
  }

  for (item of copyArr) {
    createTr(item)
  }
  table.append(tbody)
}

createStr(arrStudents)

filterWrapper.append(inpFilFio)
filterWrapper.append(inpFilFaculty)
filterWrapper.append(inpFilStartLern)
filterWrapper.append(inpFilEndLern)
container.append(filterWrapper)


for (i = 1; i <= 6; i++) {
  const label = document.createElement('label');
  const input = document.createElement('input')
  input.setAttribute('required', 'required')
  input.id = 'input' + i
  label.id = 'label' + i
  input.classList.add('inp-add-stud')
  form.append(label)
  const labNum = document.getElementById('label' + i)
  labNum.append(input)
}

document.getElementById('input1').placeholder = 'Имя'
document.getElementById('input2').placeholder = 'Фамилия'
document.getElementById('input3').placeholder = 'Отчество'
document.getElementById('input4').placeholder = 'Дата рождения'
document.getElementById('input5').placeholder = 'Дата начала обучения'
document.getElementById('input6').placeholder = 'Факультет'

let tik = 0;

const date = document.getElementById('input4')
const dateStart = document.getElementById('input5')
date.type = 'date'
dateStart.type = 'date'
form.append(btn)

btn.addEventListener ('click', function () {
  for (k = 1; k <= 6; k++) {
    const now = document.getElementById('input' + k)
    const value = now.value.trim()
    now.value = value
  if (value.length < 2 || value.length > 15) {
    if (document.getElementById('error' + k)) {
      break 
    }
    tik++

    const labNow = document.getElementById('label' + k)
    const error = document.createElement('span')
    error.id = 'error' + k
    switch (k) {
      case 1:
        error.innerHTML = 'Имя должно иметь не    менее 2-х и не более 15-ти символов';
        break;
      case 2:
        error.innerHTML = 'Фамилия должна иметь не менее 2-х и не более 15-ти символов';
        break;
      case 3:
        error.innerHTML = 'Отчество должно иметь не менее 2-х и не более 15-ти символов'
        break;
      case 4:
        error.innerHTML = 'Выберите дату рождения'
        break;
      case 5:
        error.innerHTML = 'Выберите дату начала обучения'
        break;
      case 6:
        error.innerHTML = 'Факультет должен иметь не менее 2-х и не более 15-ти символов'
        break;
    }
  labNow.append(error)
  } else {
      if (document.getElementById('error' + k)) {
      const nowError = document.getElementById('error' + k)
      nowError.remove()
      tik--
      }   
    }
}
if (tik === 0) {
  const student = {
    name: document.getElementById('input1').value,   
    surname: document.getElementById('input2').value,  
    middleName: document.getElementById('input3').value,
    DOB: document.getElementById('input4').value, 
    startLern: document.getElementById('input5').value, 
    faculty: document.getElementById('input6').value,
  }

  arrStudents.push(student)
  createStr(arrStudents)
 }
})

document.getElementById('heading1').addEventListener('click', () => {
  createStr(arrStudents.sort(( a, b ) => {
    if (a.FIO > b.FIO) {
      return 1
    }
  
    if (a.FIO < b.FIO) {
      return -1
    }
  
    return 0
  }))
})

document.getElementById('heading2').addEventListener('click', () => {
  createStr(arrStudents.sort(( a, b ) => {
    if (a.faculty > b.faculty) {
      return 1
    }
  
    if (a.faculty < b.faculty) {
      return -1
    }
  
    return 0
  }))
})

document.getElementById('heading3').addEventListener('click', () => {
  createStr(arrStudents.sort(( a, b ) => {
    if (a.DOB > b.DOB) {
      return 1
    }
  
    if (a.DOB < b.DOB) {
      return -1
    }
  
    return 0
  }))
})

document.getElementById('heading4').addEventListener('click', () => {
  createStr(arrStudents.sort(( a, b ) => {
    if (a.startLern > b.startLern) {
      return 1
    }
  
    if (a.startLern < b.startLern) {
      return -1
    }
  
    return 0
  }))
})


inpFilFio.addEventListener('input', function() {
 createStr(arrStudents)
})

inpFilFaculty.addEventListener('input', function () {
 createStr(arrStudents)
})

inpFilStartLern.addEventListener('input', function() {
 createStr(arrStudents)
})

inpFilEndLern.addEventListener('input', function() {
 createStr(arrStudents)
})
//end